[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/SunOS-Linux/anaconda)

# Anaconda (33.16.3.29-1)

Anaconda Installer for Sun/OS Linux

(C) 2021 Morales Research Corp
